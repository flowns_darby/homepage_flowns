import { Injectable } from '@angular/core';
import { ApiService } from '@service/apiService/api.service';

@Injectable({
  providedIn: 'root'
})
export class ImageUploadService {

  constructor(private api : ApiService) { }

  upload_image(image_name, image_data) {
    this.api.send_post(
      '/api/ImageUpload/flowns_imageupload',
      {
        filenm : image_name,
        img : image_data
      }
      ).toPromise().then(res =>{
        console.log(res);
      }).catch(error => {
        console.log(error);
      })
  }
}

