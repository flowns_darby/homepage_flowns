import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public server_url = "http://api.flowns.co.kr"
  headers = new Headers({
    'Content-Type': "application/x-www-form-urlencoded",
  });
  constructor(private http: Http) { }

  send_post(api_url, send_data) {
    // var body = new URLSearchParams()
    var body = "";

    var keys = Object.keys(send_data);
    keys.forEach((element,index) => {
      // body.append(element, send_data[element].toString());
      if(index == 0) {
        body += element + "=" + send_data[element];
      } else {
        body += "&" + element + "=" + send_data[element]+"";
      }
    })

    console.log(body);

    return this.http.post(
      this.server_url + api_url,
      body,
      new RequestOptions({ headers: this.headers })
    )
  }
}
