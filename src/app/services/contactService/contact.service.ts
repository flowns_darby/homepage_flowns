import { Injectable } from '@angular/core';
import { ApiService } from '../apiService/api.service';

@Injectable({
  providedIn: 'root'
})

export class ContactService {
  public list_data=[];

  constructor(private api : ApiService) { }

  get_list(page_num, page_size) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_ContactInfo_Select',
      {
        page_seq : page_num,
        page_size : page_size
      }
    ).toPromise().then(res => {
      var json = res.json().dataset.Table;
      if(json.length > 0 && json[0].ServiceCode == "00000"){
        this.list_data = this.list_data.concat(json);
      }
    })
  }

  async insert_contact(name, contact, memo) {
    var returnVal = null;

    await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_ContactInfo_Insert',
      {
        name : name,
        contact: contact,
        memo : memo
      }
    ).toPromise().then(res => {
      console.log(res.json())
      returnVal = res.json().dataset.Table;
    })

    return returnVal.length == 1 ? returnVal[0] : returnVal;
  }

  update_contact(id, is_confirm) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_ContactInfo_Update/'+id,
      {
        is_confirm : is_confirm
      }
    ).toPromise().then(res => {
      console.log(res.json())
    })
  }

  async detail_contact(id) {
    var json = null;
    await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_ContactInfo_View/'+id,
      {}
    ).toPromise().then(res => {
      json = res.json().dataset.Table
    })
    return json.length == 1? json[0] : json;
  }
}
