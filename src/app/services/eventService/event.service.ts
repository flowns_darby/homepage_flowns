import { Injectable } from '@angular/core';
import { ApiService } from'../apiService/api.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  list_data=[];

  constructor(private api : ApiService) { }

  get_list(page_num, page_size) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_EventInfo_Select',
      {
        page_seq : page_num,
        page_size: page_size
      }
    ).toPromise().then(res => {
      var json = res.json().dataset.Table;
      if(json.length != 0) {
        this.list_data = this.list_data.concat(json)
      }
    })
  }

  insert_event(event_name, worker_name, email, mobile, nickname, freebie_date) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_EventInfo_Inser',
      {
        event_name:event_name,
        worker_name: worker_name,
        email: email,
        nickname : nickname,
        freebie_date : freebie_date
      }
    ).toPromise().then(res=> {
      var json = res.json()
      console.log(json)
    })
  }

  edit_event(id, event_name, worker_name, email, mobile, nickname, freebie_date,status) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_EventInfo_Update',
      {
        id:id,
        event_name:event_name,
        worker_name: worker_name,
        email: email,
        mobile : mobile,
        nickname : nickname,
        freebie_date : freebie_date,
        status :status,
      }
    ).toPromise().then(res=> {
      var json = res.json()
      console.log(json)
    }).catch(err => {
      console.log(err);
    })
  }

  async detail_event(idx) {
    let json = null;
    await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_EventInfo_View/' + idx,
      { }
    ).toPromise().then(res => {
      json = res.json().dataset.Table
    })
    return json.length == 1? json[0] : json;
  }
}
