import { Injectable } from '@angular/core';
import { ApiService } from '../apiService/api.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private api : ApiService) { }

  async login(id, password){
    var res = await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_MemberInfo_SelectForLogin',
      {
        email : id,
        password : password
      }
    ).toPromise()
    return res.json().dataset.Table[0];
  }

  insert_user(id, password, user_name) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_MemberInfo_Inser',
      {
        username : user_name,
        email : id,
        password : password
      }
    ).toPromise().then(res => {
      console.log(res.json())
    })
  }

  update_user(idx, id, password, user_name){
    this.api.send_post(
      'api/FlownsWeb/Flowns_Web_MemberInfo_Update/'+idx,
      {
        username : user_name,
        email : id,
        password : password
      }
    ).toPromise().then(res => {
      console.log(res.json())
    })
  }

  detail_user(idx) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_MemberInfo_View' + idx,
      {}
    ).toPromise().then(res => {
      console.log(res.json())
    })
  }
}
