import { TestBed } from '@angular/core/testing';

import { RednoteService } from './rednote.service';

describe('RednoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RednoteService = TestBed.get(RednoteService);
    expect(service).toBeTruthy();
  });
});
