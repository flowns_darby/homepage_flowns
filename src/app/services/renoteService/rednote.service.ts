import { Injectable } from '@angular/core';
import { ApiService } from '../apiService/api.service';

@Injectable({
  providedIn: 'root'
})

export class RednoteService {
  public rednote_data = [];

  constructor(private api : ApiService) { }

  //데이터 값 받아서 데이터 처리하는거 처리하는거 찾아볼것
  get_list(page_num, page_size) {
    this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_RedNoteInfo_Select',
      {
        page_seq : page_num,
        page_size: page_size
      }
    // ).subscribe(res=> {
    ).toPromise().then(res =>{
      var json = res.json().dataset.Table;
      if(json.length != 0) {
        this.rednote_data = this.rednote_data.concat(json);
      } else {
        page_num--;
      }
    });

    return this.rednote_data;
  }

  async insert_rednote(user_name, title, url, image_url, content) {
    var return_val = await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_RedNoteInfo_Insert',
      {
        username : user_name, 
        title : title,
        nv_url : url,
        attachment : image_url,
        content: content
      }
    ).toPromise().then(res => {
      var json = res.json();      
      return (json.dataset.Table.length == 1 ? json.dataset.Table[0] : json.dataset.Table);
    }).catch().then(error => {
      return error;
    });

    return return_val;
  }

  async edit_rednote(idx, user_name, title, url, image_url, content, status) {
    // var return_val = null;
    var return_val = await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_RedNoteInfo_Update',
      {
        id : idx,
        username : user_name, 
        title : title,
        nv_url : url,
        attachment : image_url,
        content: content
      }
    ).toPromise().then(res => {
      var json = res.json();
      return (json.dataset.Table.length == 1 ? json.dataset.Table[0] : json.dataset.Table);
    }).catch().then(error => {
      return error;
    });

    return return_val;
  }

  async detail_rednote(idx:string){
    let json=null
    await this.api.send_post(
      '/api/FlownsWeb/Flowns_Web_RedNoteInfo_View/' + idx,
      { }
    ).toPromise().then(res => {
      json = res.json().dataset.Table;
    });
    return json.length == 1 ? json[0] : json;
  }
}
