import { Injectable } from '@angular/core';
import { ApiService } from '@service/apiService/api.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private api : ApiService) { }

  async upload_image(image_name, image_data) {
    var return_val = null;
    await this.api.send_post(
      '/api/ImageUpload/flowns_imageupload',
      {
        filenm : image_name,
        img : encodeURIComponent(image_data)
      }
    ).toPromise().then(res =>{
      return_val = res.json();
    }).catch(error => {
      console.log(error);
      return_val = error;
    });
    return return_val;
  }
}
