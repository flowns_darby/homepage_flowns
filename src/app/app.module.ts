import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpModule } from '@angular/http';

import { ApiService } from '@service/apiService/api.service';
import { SafeHtmlPipe } from './pipe/safe_html/safe-html.pipe';

import {MatDialogModule} from "@angular/material";
import {InstafeedModalComponent} from '@component/instafeed-modal/instafeed-modal.component';

@NgModule({
  declarations: [
    AppComponent, 
    // SafeHtmlPipe,
    InstafeedModalComponent,
  ],
  entryComponents: [
    InstafeedModalComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    MatDialogModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ApiService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
