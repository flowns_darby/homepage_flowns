import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruitPage } from './recruit.page';

describe('RecruitPage', () => {
  let component: RecruitPage;
  let fixture: ComponentFixture<RecruitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruitPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
