import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistoryPage } from './history.page';

import { HeaderComponent } from '@component/header/header.component';
import { FooterComponent } from '@component/footer/footer.component';
import {HeaderMobileComponent} from '@component/header-mobile/header-mobile.component';

const routes: Routes = [
  {
    path: '',
    component: HistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HistoryPage,
    HeaderComponent,
    FooterComponent,
    HeaderMobileComponent
  ]
})
export class HistoryPageModule {}
