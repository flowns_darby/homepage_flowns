import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  click_popup01() {
    var popup = (<HTMLDivElement>document.querySelector('.home_popup01'));
    popup.style.display = 'none';
  }
  click_popup02() {
    var popup = (<HTMLDivElement>document.querySelector('.home_popup02'));
    popup.style.display = 'none';
  }
  click_popup03() {
    var popup = (<HTMLDivElement>document.querySelector('.home_popup03'));
    popup.style.display = 'none';
  }
}


