import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlogramPage } from './flogram.page';

describe('FlogramPage', () => {
  let component: FlogramPage;
  let fixture: ComponentFixture<FlogramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlogramPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlogramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
