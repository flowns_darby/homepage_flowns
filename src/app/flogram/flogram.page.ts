import { Component, OnInit } from '@angular/core';
// import * as $ from '../../assets/js/jquery.min';
// import * as jQuery from '../../assets/js/jquery.min';
import * as Instafeed from '../../assets/js/instafeed.min';
// import '../../assets/js/instafeed.min';
// import * as bPopup from '../../assets/js/jquery.bpopup.min';
// import '../../assets/js/jquery.bpopup.min';
import { ModalController, PopoverController  } from '@ionic/angular';
// import { InstafeedModalComponent } from '@component/instafeed-modal/instafeed-modal.component';
import {InstafeedModalComponent} from '../component/instafeed-modal/instafeed-modal.component';

@Component({
  selector: 'app-flogram',
  templateUrl: './flogram.page.html',
  styleUrls: [
    './flogram.page.scss',
  ],
})
export class FlogramPage implements OnInit {
  constructor(public modalController : ModalController, public popoverController : PopoverController) { }

  ngOnInit() {
    const this_page = this;
    var loadButton = document.getElementById('load-more');
    var insta = new Instafeed({
      target: 'instagram',
      get: 'user',
      clientId: 'flowns_',
      userId: '8232124718',
      accessToken: '8232124718.8027574.87ee3d3674334d3cb8a9e0c050fb81ee',
      sortBy: 'most-recent',
      limit: 15,
      filter: function (feed) {
        document.querySelector("#instagram").insertAdjacentHTML('beforeend','<div class="instafeed" style="margin-bottom:20px;" href="' + feed.link + '"><img src="' + feed.images.low_resolution.url + '" /></div>');
      },
      after: function () {
        var el = document.getElementById('instagram');
        if (el.classList){
          el.classList.add('show');
        } else {
          el.className += ' ' + 'show';
        }
        
        var feed_list = document.querySelectorAll(".instafeed");
        for(var i=0; i<feed_list.length; i++) {
          feed_list[i].addEventListener('click', async function() {
            var feed_url = this.getAttribute("href");
            await this_page.presentModal(feed_url+"embed");
          });
        }
      }
    });
    insta.run();
  }

  async presentModal(url) {
    var modal = await this.modalController.create({
      component : InstafeedModalComponent,
      componentProps : {
        url : url
      },
      cssClass : "instafeed_modal"
    })
    return await modal.present();
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: InstafeedModalComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}