import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RednotePage } from './rednote.page';

import { HeaderComponent } from '@component/header/header.component';
import { FooterComponent } from '@component/footer/footer.component';
import {HeaderMobileComponent} from '@component/header-mobile/header-mobile.component';

import { SafeHtmlPipe } from '../pipe/safe_html/safe-html.pipe';

const routes: Routes = [
  {
    path: '',
    component: RednotePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    RednotePage,
    HeaderComponent,
    FooterComponent,
    HeaderMobileComponent,
    SafeHtmlPipe
  ],
})
export class RednotePageModule {}
