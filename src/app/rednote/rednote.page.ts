import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { RednoteService } from '@service/renoteService/rednote.service';

@Component({
  selector: 'app-rednote',
  templateUrl: './rednote.page.html',
  styleUrls: ['./rednote.page.scss'],
})

export class RednotePage implements OnInit {
  page_num = 1;
  page_size = 10;
  
  rednote_list = [];

  constructor(private rednote : RednoteService) { }

  ngOnInit() {
    this.rednote_list = this.rednote.get_list(this.page_num, this.page_size);
  }

  scrollPaging(event) {
    // var total_height = event.detail.event.path[0].scrollHeight - event.detail.scrollTop;
    // if(total_height <= event.target.offsetHeight) {
    //   this.page_num++;
    //   this.rednote_list = this.rednote.get_list(this.page_num,this.page_size);
    // }
    var total_height = event.currentTarget.children[0].scrollHeight - event.detail.scrollTop;
    if(total_height <= event.target.offsetHeight) {
      this.page_num++;
      this.rednote_list = this.rednote.get_list(this.page_num,this.page_size);
    }
  }

  

 


  fn_htmldecode(val) {
    var replaceMap = {"&amp;":"&", "&lt;":"<", "&gt;":">", "&quot;":"\"", "&#39;":"'"};
    var repRegExp = new RegExp(Object.keys(replaceMap).join("|"), "gi");
    val = val.replace(repRegExp, replaceFunc);

    function replaceFunc(val){
      return replaceMap[val];
    }
    
    val = val.replace(/(<([^>]+)>)/ig,"");
    
    return val;
  }

}