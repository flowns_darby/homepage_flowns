import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RednotePage } from './rednote.page';

describe('RednotePage', () => {
  let component: RednotePage;
  let fixture: ComponentFixture<RednotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RednotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RednotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
