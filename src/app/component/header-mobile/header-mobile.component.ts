import { Component, OnInit, HostListener } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-header-mobile',
  templateUrl: './header-mobile.component.html',
  styleUrls: ['./header-mobile.component.scss'],
})
export class HeaderMobileComponent implements OnInit {

  constructor(private menu : MenuController) {}

  ngOnInit() {
    var path_name = location.pathname.replace(/\//gi, "");
    this.active_menu(path_name);
  }

  openMobileMenu() {
    this.menu.open("mobile_header");
  }

  closeMobileMenu() {
    this.menu.close("mobile_header");
  }

  toggleMobileMenu() {
    this.menu.open('mobile_header');
  }

  sideMenu(event) {
    console.log(event);
    var target = event.target;

    // +-버튼 눌렷을때 부모로 target 변경
    if(event.target.className.indexOf("snb_icon") != -1) {
      target = event.target.parentElement;
    }

    //서브 매뉴 정보 없을때
    if(target.getAttribute("data-sideMenu") == null) {
      return;
    }

    var target_id = target.getAttribute("data-sideMenu");
    var target_element = document.getElementById(target_id);

    /* 선택된거랑 다르면 열린거 닫기 */
    var side_menu_list = document.querySelectorAll(".side_menu");
    var snb_icon_list = document.querySelectorAll(".snb_icon");
    var sub_menu_idx = null;
    for(var i=0; i<side_menu_list.length; i++) {
      var element = <HTMLElement>side_menu_list[i];
      var snb_icon = <HTMLElement>snb_icon_list[i];
      if(target_element != element) {
        element.style.height="0px";
        snb_icon.textContent = "+";
      } else {
        sub_menu_idx = i;
      }
    }

    /* 선택된게 열거나 닫거나 */
    var sub_icon = <HTMLElement>snb_icon_list[sub_menu_idx];
    if(target_element.style.height == "" || target_element.style.height == "0px") {
      target_element.style.height="22vw";
      sub_icon.textContent = "-";
    } else {
      target_element.style.height="0px";
      sub_icon.textContent = "+";
    }
  }

  // display show or hide
  change_display(target_element, is_show) {
    if(is_show) {
      target_element.style.display="";
    } else {
      target_element.style.display="none";
    }
  }

  // resize function
  @HostListener('window:resize', ['$event'])
  onResize(event){
    if(window.innerWidth <= 850) {
      this.change_display(document.querySelector("app-header-mobile"),true);
    } else {
      this.change_display(document.querySelector("app-header-mobile"),false);
    }
  }
  
  active_menu(pathname) {
    var mobile_menu_1_depth_list = document.querySelectorAll(".snb_list>div");
    if(["about","history"].indexOf(pathname) != -1) {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[0];
      mobile_menu_1_depth.classList.add("active_menu");
      this.active_sub_menu(pathname);
    } else if(["flownser","recruit"].indexOf(pathname) != -1) {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[1];
      mobile_menu_1_depth.classList.add("active_menu");
      this.active_sub_menu(pathname);
    } else if(["rednote", "flogram"].indexOf(pathname) != -1) {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[2];
      // mobile_menu_1_depth.classList.add("active_menu");
      // this.active_sub_menu(pathname);
      mobile_menu_1_depth.children[0].classList.add("active_menu");
    } else if(pathname == "gongsapd") {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[3];
      mobile_menu_1_depth.children[0].classList.add("active_menu");
    } else if(pathname == "board") {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[4];
      mobile_menu_1_depth.children[0].classList.add("active_menu");
    } else if(pathname == "contact") {
      var mobile_menu_1_depth = <HTMLElement>mobile_menu_1_depth_list[5];
      mobile_menu_1_depth.children[0].classList.add("active_menu");
    }
  }
  
  active_sub_menu(pathname) {
    var sub_menu_a_list = document.querySelectorAll(".side_menu a");
    for(var i=0; i<sub_menu_a_list.length;i++) {
      var a_tag = <HTMLElement>sub_menu_a_list[i];
      if(a_tag.getAttribute("href").indexOf(pathname) != -1){
        a_tag.classList.add("active_menu");
      }
    }
  }
}
