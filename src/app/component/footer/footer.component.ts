import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  footer_toggle() {
    // const tg = event.target;
    const tg = (<HTMLDivElement>document.querySelector('.footer04 .rela_site'));
    const is_on = tg.classList.contains('on');
    is_on ? tg.classList.remove('on') : tg.classList.add('on') ;
  }
}
