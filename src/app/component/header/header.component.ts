import { Component, OnInit, HostListener, Renderer } from '@angular/core';
import { Platform, AngularDelegate } from '@ionic/angular';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(public platform: Platform, public renderer : Renderer) { }

  origin_url = location.pathname.substring(location.pathname.lastIndexOf("/")+1)

  ngOnInit() {
    // if main page
    if(location.pathname == "/home" || location.pathname == "/") {
      this.change_display(document.querySelector("app-header"),false)
    }

    // if android or ios
    if(this.platform.is("android") || this.platform.is("ios")) {
      this.change_display(document.querySelector("app-header"),true);
    }

    this.set_url_active_menu(this.origin_url);
  }

  // set active menu for now url
  set_url_active_menu(url) {
    // document.querySelectorAll(".s_"+url).forEach(element => {
    //   element.classList.add("active_menu");
    // })
    var actvie_menu = document.querySelectorAll(".s_"+url);
    // [].forEach.call(actvie_menu, element => {
    //   element.classList.add("active_menu");
    // })
    for(var i=0; i<actvie_menu.length; i++) {
      actvie_menu[i].classList.add("active_menu")
    }
  }

  // resize function
  @HostListener('window:resize', ['$event'])
  onResize(event){
    if(window.innerWidth >= 850) {
      this.change_display(document.querySelector("app-header"),true);
    } else {
      this.change_display(document.querySelector("app-header"),false);
    }
  }

  mouse_over(event) {
    this.all_top_menu_deactive()
    
    // top menu
    if(event.target.classList.contains("top_menu")) {
      let top_idx = "";
      var search_tag = "top0";
      
      // search top_idx and this add active_menu
      var class_list = event.target.classList;
      // [].forEach.call(class_list, element => {
      //   if(element.search(search_tag) != -1) {
      //     top_idx = element.replace(search_tag,"");
      //   }
      // })
      for(var i=0; i<class_list.length;i++) {
        if(class_list[i].search(search_tag) != -1) {
          top_idx = class_list[i].replace(search_tag,"");
        }
      }
      document.querySelector(".top0"+top_idx).classList.add("active_menu")
      
      // sub_menu all hide
      this.all_sub_menu_hide();
      
      // top -> sub_menu show
      if([1,2].includes(Number(top_idx))){
        var slide_element = document.getElementById("sub_drop0"+ top_idx);
        this.bg_show(true);
        slide_element.classList.add('drop_wrap_show')
      } else {
        this.bg_show(false);
      }
    } else if(event.target.classList.contains("sub_menu")) {
      this.all_sub_menu_deactive();
      event.target.classList.add("active_menu");
    }
    this.set_url_active_menu(this.origin_url);
  }

  mouse_leave() {
    this.all_top_menu_deactive()
    this.all_sub_menu_hide();
    this.bg_show(false);
    this.set_url_active_menu(this.origin_url);
  }

  // display show or hide
  change_display(target_element, is_show) {
    if(is_show) {
      target_element.style.display="";
    } else {
      target_element.style.display="none";
    }
  }

  // remove top_menu_active
  all_top_menu_deactive(){
    // document.querySelectorAll(".top_menu").forEach(element => {
    //   element.classList.remove("active_menu");
    // })
    // var top_menus = document.querySelectorAll(".top_menu");
    // [].forEach.call(top_menus, element => {
    //   element.classList.remove("active_menu");
    // })
    var top_menus = document.querySelectorAll(".top_menu");
    for(var i=0; i<top_menus.length;i++) {
      top_menus[i].classList.remove("active_menu");
    }
  }

  all_sub_menu_deactive() {
    // document.querySelectorAll(".sub_menu").forEach(element => {
    //   element.classList.remove("active_menu")
    // })
    var sub_menus = document.querySelectorAll(".sub_menu");
    // [].forEach.call(sub_menus, element => {
    //   element.classList.remove("active_menu");
    // })
    for(var i=0; i<sub_menus.length;i++) {
      sub_menus[i].classList.remove("active_menu");
    }
  }

  // hide all sub_menu
  all_sub_menu_hide() {
    var sub_menus = document.querySelectorAll(".drop_wrap");
    // [].forEach.call(sub_menus, element => {
    //   // element.style.display="none";
    //   element.classList.remove('drop_wrap_show')
    // })
    for(var i=0; i<sub_menus.length;i++) {
      sub_menus[i].classList.remove("drop_wrap_show");
    }
  }

  bg_show(is_show) {
    if(is_show) {
      document.querySelector(".bg").classList.add("bg_ani")
    } else {
      document.querySelector(".bg").classList.remove("bg_ani")
    }
  }
}
