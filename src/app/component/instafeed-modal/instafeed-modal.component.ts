import { Component, OnInit } from '@angular/core';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';

@Component({
  selector: 'app-instafeed-modal',
  templateUrl: './instafeed-modal.component.html',
  styleUrls: ['./instafeed-modal.component.scss'],
})
export class InstafeedModalComponent implements OnInit {

  url : string;
  urlSafe: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) {
  }
  
  async ngOnInit() {
    console.log(this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    console.log(this.urlSafe);
  }
}
