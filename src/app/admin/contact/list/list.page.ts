import { Component, OnInit } from '@angular/core';
import { ContactService } from '@service/contactService/contact.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  page_num = 1;
  page_size = 20;

  constructor(public contact: ContactService) { }

  ngOnInit() {
    this.contact.get_list(this.page_num, this.page_size);
  }

  move_url(url:string) {
    location.href = url;
  }

  scrollPaging(event) {
    // var total_height = event.detail.event.path[0].scrollHeight - event.detail.scrollTop;
    // if(total_height <= event.target.offsetHeight) {
    //   this.page_num++;
    //   this.contact.get_list(this.page_num, this.page_size);
    // }
    var total_height = event.currentTarget.children[0].scrollHeight - event.detail.scrollTop;
    if(total_height <= event.target.offsetHeight) {
      this.page_num++;
      this.contact.get_list(this.page_num,this.page_size);
    }
  }
  scrollPaging_div(event) {
    var total_height = event.target.scrollHeight - event.target.scrollTop;
    if(total_height <= event.target.offsetHeight) {
      this.page_num++;
      this.contact.get_list(this.page_num,this.page_size);
    }
  }
}
