import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '@service/contactService/contact.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  is_readonly:boolean;
  id:string;
  mode:string;
  data: any;

  constructor( private route : ActivatedRoute, private contact : ContactService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.mode = this.route.snapshot.paramMap.get('mode');

    if(this.mode=="detail") {
      this.is_readonly = true;
    } else {
      this.is_readonly = false;
    }
  }

  async ngOnInit() {
    this.data = await this.contact.detail_contact(this.id);
    console.log(this.data);
  }

  button_action(event) {
    if(this.mode == "detail") {
      location.href = "/admin/contact/edit/"+this.data.id
    } else if(this.mode == "edit") {
      var is_confirm = document.querySelector("#is_confirm_select") as HTMLSelectElement;
      this.contact.update_contact(this.id, eval(is_confirm.value));
      history.go(-1);
    } else if(this.mode == "insert") {
      alert("등록")
    }
  }
}
