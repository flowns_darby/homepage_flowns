import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { UserService } from '@service/userService/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // @ViewChild('id') id : ElementRef
  // @ViewChild('password') password : ElementRef;
  email : string = "";
  password : string = "";

  constructor(private user : UserService) { }

  ngOnInit() { }

  async login() {
    var json = await this.user.login(this.email, this.password);
    if(json.ServiceCode == "00000") {
      sessionStorage.setItem("flowns_admin_user", JSON.stringify(json))
      location.href = "/admin/rednote/list"
    }
  }
}