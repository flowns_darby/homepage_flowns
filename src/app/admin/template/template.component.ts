import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'admin-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    if(sessionStorage.getItem("flowns_admin_user")==null || JSON.parse(sessionStorage.getItem("flowns_admin_user")).ServiceCode!="00000") {
      location.replace("/admin/login");
    }
    this.set_active();
  }

  mouse_over(event) {
    var target_element = event.target
    if(target_element.classList.contains("menu_normal")) {
      target_element.classList.remove("menu_normal");
      target_element.classList.add("menu_over");
    }
  }

  mouse_leave(event) {
    var target_element = event.target
    if(target_element.classList.contains("menu_over")) {
      target_element.classList.remove("menu_over");
      target_element.classList.add("menu_normal");
    }
  }

  set_active() {
    var url = location.pathname;
    console.log(url);
    if(url.search("rednote") != -1) {
      document.querySelector("#menu_rednote").classList.remove("menu_normal");
      document.querySelector("#menu_rednote").classList.add("menu_active");
    } else if(url.search("event") != -1 ) {
      document.querySelector("#menu_event").classList.remove("menu_normal");
      document.querySelector("#menu_event").classList.add("menu_active");
    } else if(url.search("contact") != -1) {
      document.querySelector("#menu_contact").classList.remove("menu_normal");
      document.querySelector("#menu_contact").classList.add("menu_active");
    }
  }
}
