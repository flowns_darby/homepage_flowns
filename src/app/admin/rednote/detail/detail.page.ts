import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RednoteService } from '@service/renoteService/rednote.service';
import { ImageService } from '@service/imageService/image.service';
import * as commonjs from '@js/common.js'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  id:string;
  mode:string;
  data: any;
  is_readonly:boolean;
  @ViewChild("action_button", { read: ElementRef }) action_button : ElementRef;
  image_data:any = null;

  constructor(private route : ActivatedRoute, private rednote : RednoteService, private image : ImageService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.mode = this.route.snapshot.paramMap.get('mode');

    if(this.mode=="detail") {
      this.is_readonly = true;
    } else {
      this.is_readonly = false;
    }
   }

  async ngOnInit() {
    if(this.mode != "insert") {
      this.data = await this.rednote.detail_rednote(this.id)
      if((typeof(this.data.ServiceCode) != "undefined" && this.data.ServiceCode != "00000") ||  typeof(this.data.ServiceCode) == "undefined") {
        alert("불러오는데 에러 발생");
        history.go(-1);
      }
    }
    this.button_change(this.mode);
    console.log(this.data);
    if(commonjs.urlExists(this.data.attachment)) {
      if(this.image_data == null) {
        this.image_data = new Object();
        this.image_data["data"] = this.data.attachment;
        this.image_data["image_url"] = this.data.attachment;
      }
      
    }
  }

  button_change(mode) {
    if(mode=="insert") {
      this.action_button.nativeElement.textContent = "등록";
    } else if(mode=="edit") {
      this.action_button.nativeElement.textContent = "수정 완료";
    } else if(mode=="detail") {
      this.action_button.nativeElement.textContent ="수정";
    }
  }

  async button_action(event) {
    if(this.mode == "detail") {
      console.log("detail");
      location.href = "/admin/rednote/edit/"+this.data.id
    } else if(this.mode == "edit") {
      console.log("edit");
      if(this.image_data.data != this.data.attachment){
        if(this.image_data != null && typeof(this.image_data.data) != "undefined" && typeof(this.image_data.image_url) == "undefined") {
          var image_upload_json = await this.image.upload_image(this.image_data.name, this.image_data.data);
          if(typeof(image_upload_json.rsltCd) != "undefined" && image_upload_json.rsltCd == "succ") {
            this.image_data["image_url"] = image_upload_json.rsltImgUrl;
          } else {
            alert("이미지 등록에 실패하였습니다.");
            return;
          }
        } else if(this.image_data != null && typeof(this.image_data.image_url) != "undefined") {
          console.log("alreay image upload");
        } else {
          alert("이미지를 등록하세요");
          return;
        }
      } else {
        console.log("image not change");
      }

      var user_name = JSON.parse(sessionStorage.getItem("flowns_admin_user")).username;
      var title = document.getElementById("title") as HTMLInputElement;
      var content = <HTMLInputElement> document.getElementById("content");
      var nv_url = <HTMLInputElement> document.getElementById("nv_url");
      if(title.value == "" || content.value == "" || nv_url.value == "" ) {
        alert("값을 입력하세요");
        return;
      } else if(typeof(this.image_data["image_url"]) != "undefined" && this.image_data["image_url"] != "") {
        var rednote_json = await this.rednote.edit_rednote(this.data.id, user_name, title.value, nv_url.value, this.image_data["image_url"],content.value, "true");
        if(rednote_json != null && typeof(rednote_json.ServiceCode) != "undefined" && rednote_json.ServiceCode == "00000") {
          history.go(-1);
        } else {
          alert("등록에 실패하였습니다");
          return;
        }
      } else {
        alert("이미지 등록에 실패하였습니다");
        return;
      }
    } else if(this.mode == "insert") {
      console.log("insert");
      if(this.image_data != null && typeof(this.image_data.data) != "undefined" && typeof(this.image_data.image_url) == "undefined") {
        var image_upload_json = await this.image.upload_image(this.image_data.name, this.image_data.data);
        if(typeof(image_upload_json.rsltCd) != "undefined" && image_upload_json.rsltCd == "succ") {
          this.image_data["image_url"] = image_upload_json.rsltImgUrl;
        } else {
          alert("이미지 등록에 실패하였습니다.");
        }
      } else if(this.image_data != null && typeof(this.image_data.image_url) != "undefined") {
        console.log("alreay image upload");
      } else {
        alert("이미지를 등록하세요");
        return;
      }

      var user_name = JSON.parse(sessionStorage.getItem("flowns_admin_user")).username;
      var title = document.getElementById("title") as HTMLInputElement;
      var content = <HTMLInputElement> document.getElementById("content");
      var nv_url = <HTMLInputElement> document.getElementById("nv_url");
      if(title.value == "" || content.value == "" || nv_url.value == "" ) {
        alert("값을 입력하세요");
        return;
      } else if(typeof(this.image_data["image_url"]) != "undefined" && this.image_data["image_url"] != "") {
        var rednote_json = await this.rednote.insert_rednote(user_name, title.value, nv_url.value, this.image_data["image_url"], content.value);        
        if(rednote_json != null && typeof(rednote_json.ServiceCode) != "undefined" && rednote_json.ServiceCode == "00000") {
          history.go(-1);
        } else {
          alert("등록에 실패하였습니다");
          return;
        }
      } else {
        alert("이미지 등록에 실패하였습니다");
        return;
      }
    }
  }

  fileChange(event) {
    const this_page = this;
    if(event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      var file_name = event.target.files[0].name;
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = function(reader_event:any) {
        this_page.image_data= {
          data : reader_event.target.result,
          name : file_name
        };
      }
    } else {
      if(this_page.mode == "edit") {
        this_page.image_data = new Object();
        this_page.image_data["data"] == this_page.data.attachment;
        this.image_data["image_url"] = this.data.attachment;
      } else {
        this_page.image_data = null;
      }
    }
  }
}