import { Component, OnInit } from '@angular/core';
import { EventService } from '@service/eventService/event.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  page_num = 1;
  page_size = 20;

  constructor(public event :EventService) { }

  ngOnInit() {
    this.event.get_list(this.page_num,this.page_size);
  }
  
  move_url(url) {
    location.href = url;
  }

  scrollPaging(event) {
    // var total_height = 0;
    // if(typeof(event.detail.event.path) != "undefined") {
    //   total_height = event.detail.event.path[0].scrollHeight - event.detail.scrollTop;
    // } else {
    //   total_height = event.detail.event.target.scrollHeight - event.detail.scrollTop;
    // }
    var total_height = event.currentTarget.children[0].scrollHeight - event.detail.scrollTop;
    if(total_height <= event.target.offsetHeight) {
      this.page_num++;
      this.event.get_list(this.page_num,this.page_size);
    }
  }

  scrollPaging_div(event) {
    var total_height = event.target.scrollHeight - event.target.scrollTop;
    if(total_height <= event.target.offsetHeight) {
      this.page_num++;
      this.event.get_list(this.page_num,this.page_size);
    }
  }
}
