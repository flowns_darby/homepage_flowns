import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '@service/eventService/event.service'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  id:string;
  mode:string;
  data: any;
  is_readonly:boolean;
  @ViewChild("action_button", { read: ElementRef }) 
  public action_button : ElementRef

  constructor(private route : ActivatedRoute, private event : EventService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.mode = this.route.snapshot.paramMap.get('mode');

    if(this.mode=="detail") {
      this.is_readonly = true;
    } else {
      this.is_readonly = false;
    }
    
  }
  
  async ngOnInit() {
    if(Number(this.id) != 0)
      this.data = await this.event.detail_event(this.id)
    
    if(this.mode == "detail") {
      this.action_button.nativeElement.textContent = "수정";
    } else {
      this.action_button.nativeElement.textContent = "저장";
    }

    this.button_change(this.mode);
  }

  button_change(mode) {
    if(mode=="insert") {
      this.action_button.nativeElement.textContent = "등록";
    } else if(mode=="edit") {
      this.action_button.nativeElement.textContent = "수정 완료";
    } else if(mode=="detail") {
      this.action_button.nativeElement.textContent ="수정";
    }
  }

  button_action(event) {
    if(this.mode == "detail") {
      location.href = "/admin/event/edit/"+this.data.id
    } else if(this.mode == "edit") {
      var freebie_date = null;
      var status = <HTMLSelectElement>document.getElementById("is_status_select");
      if(status.value=="SEND") {
        freebie_date = new Date().toJSON().split("T")[0];
      }
      console.log(freebie_date != null?freebie_date:"");
      this.event.edit_event(
        this.id, 
        this.data.event_name, 
        this.data.worker_name, 
        this.data.email, 
        this.data.mobile, 
        this.data.nickname,
        freebie_date != null?freebie_date:"",
        status.value
      );
      // history.go(-1);
    } else if(this.mode == "insert") {
      alert("등록")
    }
  }
}
