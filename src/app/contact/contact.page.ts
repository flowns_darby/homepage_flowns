import { Component, OnInit } from '@angular/core';
import { ContactService } from '@service/contactService/contact.service'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  name_input :string;
  contact_input : string;
  memo_input : string;

  constructor(private contact : ContactService) { }

  ngOnInit() {
  }

  async submit() {
    var is_null = false;
    var check_element_name = ["name", "contact", "memo"];
    check_element_name.forEach(element => {
      if(this.check_null(element)) {
        is_null = true;
      }
    })

    if(is_null == false) {
      var json = await this.contact.insert_contact(this.name_input,this.contact_input, this.memo_input);
      console.log(json)
      if(typeof(json.ServiceCode) != "undefined" && json.ServiceCode == "00000") {
        this.name_input = this.contact_input = this.memo_input = "";
        alert("등록되었습니다.")
      } else {
        alert("등록에 실패하였습니다.");
      }
    } else {
      alert("빈값을 입력하세요.")
    }
  }

  check_null(element_id) {
    var check_element = (<HTMLInputElement>document.getElementById(element_id));
    if(check_element.value == "")
      return true;
    else 
      return false;
  }
}
