import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'flownser', loadChildren: './flownser/flownser.module#FlownserPageModule' },
  { path: 'rednote', loadChildren: './rednote/rednote.module#RednotePageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  { path: 'recruit', loadChildren: './recruit/recruit.module#RecruitPageModule' },
  { path: 'flogram', loadChildren: './flogram/flogram.module#FlogramPageModule' },
  { path: 'gongsapd', loadChildren: './gongsapd/gongsapd.module#GongsapdPageModule' },
  { path: 'board', loadChildren: './board/board.module#BoardPageModule' },
  { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  { path: 'admin', redirectTo: 'admin/login', pathMatch: 'full' },
  { path: 'admin/login', loadChildren: './admin/login/login.module#LoginPageModule' },
  { path: 'admin/rednote/list', loadChildren: './admin/rednote/list/list.module#ListPageModule' },
  { path: 'admin/rednote/:mode/:id', loadChildren: './admin/rednote/detail/detail.module#DetailPageModule' },
  { path: 'admin/event/list', loadChildren: './admin/event/list/list.module#ListPageModule' },
  { path: 'admin/event/:mode/:id', loadChildren: './admin/event/detail/detail.module#DetailPageModule' },
  { path: 'admin/contact/list', loadChildren: './admin/contact/list/list.module#ListPageModule' },
  { path: 'admin/contact/:mode/:id', loadChildren: './admin/contact/detail/detail.module#DetailPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
