import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GongsapdPage } from './gongsapd.page';

import { HeaderComponent } from '@component/header/header.component';
import { FooterComponent } from '@component/footer/footer.component';
import {HeaderMobileComponent} from '@component/header-mobile/header-mobile.component';

const routes: Routes = [
  {
    path: '',
    component: GongsapdPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    GongsapdPage, 
    HeaderComponent, 
    FooterComponent, 
    HeaderMobileComponent
  ]
})
export class GongsapdPageModule {}
