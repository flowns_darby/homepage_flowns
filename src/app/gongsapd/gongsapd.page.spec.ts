import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GongsapdPage } from './gongsapd.page';

describe('GongsapdPage', () => {
  let component: GongsapdPage;
  let fixture: ComponentFixture<GongsapdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GongsapdPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GongsapdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
