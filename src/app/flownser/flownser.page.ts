import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flownser',
  templateUrl: './flownser.page.html',
  styleUrls: ['./flownser.page.scss'],
})
export class FlownserPage implements OnInit {
  data_list = [
    {
      "title" : "경영",
      "sub_title" : "공사PD를 소비자 마음속에 남기고자 열심히 뛰어다닙니다.",
      "flownsers" : [
        {
          "name" : "Boss",
          "info" : "보호받고 싶은 보스",
          "explanation" : "보호 해주세요",
          "profile" : "/images/flownser01.jpg",
          "characters" : "/images/flownser_ch01.png"
        },
        {
          "name" : "Andrew",
          "info" : "로맨틱가이",
          "explanation" : "고전은 영원하다",
          "profile" : "/images/flownser03.jpg",
          "characters" : "/images/flownser_ch03.png"
        },
        {
          "name" : "Daniel",
          "info" : "음악을 주도하는 리듬",
          "explanation" : "즐겁게 리듬에 맡기자",
          "profile" : "/images/flownser_daniel.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
      ]
    },
    {
      "title" : "경영관리본부",
      "sub_title" : "비가오나 눈이오나 지원하겠습니다.",
      "flownsers" : [
        {
          "name" : "Papa",
          "info" : "파파 스머프",
          "explanation" : "금고를 꽉꽉 채워주세요",
          "profile" : "/images/flownser02.jpg",
          "characters" : "/images/flownser_ch02.png"
        },
        {
          "name" : "Chloe",
          "info" : "매력자본가",
          "explanation" : "계속 봐주면 더 빛난다~!",
          "profile" : "/images/flownser_chloe.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Henry",
          "info" : "아직도 서툰 투덜이",
          "explanation" : "가급적 대화로 해결을...",
          "profile" : "/images/flownser_henry.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Sharon",
          "info" : "스벅마니아",
          "explanation" : "신비한 스타벅스 음료~~",
          "profile" : "/images/flownser32.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Ronnie",
          "info" : "무인도 등대",
          "explanation" : "로니 콜먼을 위하여!",
          "profile" : "/images/flownser_ronnie.png",
          "characters" : "/images/flownser_ch00.png"
        },
      ]
    },
    {
      "title" : "사업본부",
      "sub_title" : "꿈꾸는 공간, 하고 싶은 일, 내가 이루어 줄게!",
      "flownsers" : [
        {
          "name" : "Ben",
          "info" : "백설 가이",
          "explanation" : "언젠가 시간은 거꾸로 흐를꺼야",
          "profile" : "/images/flownser_ben.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Olley",
          "info" : "상냥한 사냥꾼",
          "explanation" : "해치지않아요 :^)",
          "profile" : "/images/flownser19.jpg",
          "characters" : "/images/flownser_ch19.png"
        },
        {
          "name" : "Dobby",
          "info" : "ID a BANK",
          "explanation" : "자라나라 머리머리! 떠올라라 아이디어!",
          "profile" : "/images/flownser_dobby.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Sammie",
          "info" : "바쁜 집수니",
          "explanation" : "어떻게든 됩니다",
          "profile" : "/images/flownser_sammie.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Grace",
          "info" : "따뜻하고 밝은 해",
          "explanation" : "사랑합니다. 사랑하세요",
          "profile" : "/images/flownser35.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Jane",
          "info" : "슈가마니아",
          "explanation" : "당이 필요해",
          "profile" : "/images/flownser33.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Aria",
          "info" : "감사하는 삶",
          "explanation" : "Thank u. next",
          "profile" : "/images/flownser_aria.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
      ]
    },
    {
      "title" : "기술연구소",
      "sub_title" : "printf(\"내일을 짓는 플로언스 !!\");",
      "flownsers" : [
        {
          "name" : "James",
          "info" : "일단 GO ~ 재밌으면 OK",
          "explanation" : "화이팅",
          "profile" : "/images/flownser25.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Ellie",
          "info" : "외로운 사과장수",
          "explanation" : "사과..맛없다",
          "profile" : "/images/flownser12.jpg",
          "characters" : "/images/flownser_ch12.png"
        },
        {
          "name" : "Raphael",
          "info" : "개발의 대천사님",
          "explanation" : "만능이 되고 싶다",
          "profile" : "/images/flownser15.jpg",
          "characters" : "/images/flownser_ch15.png"
        },
        {
          "name" : "Kyla",
          "info" : "방패 마스터",
          "explanation" : "수정사항을 방어한다",
          "profile" : "/images/flownser16.jpg",
          "characters" : "/images/flownser_ch16.png"
        },
        {
          "name" : "Claire",
          "info" : "빨간망토",
          "explanation" : "따스하다..",
          "profile" : "/images/flownser17.jpg",
          "characters" : "/images/flownser_ch17.png"
        },
        {
          "name" : "Darby",
          "info" : "보라돌이",
          "explanation" : "보라색 밖에 없는 책상은 제 책상입니다.",
          "profile" : "/images/flownser31.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "D.dog",
          "info" : "캐릭터 자판기",
          "explanation" : "뾰로롱뾰로롱 삐빠빠룰랄라!",
          "profile" : "/images/flownser10.jpg",
          "characters" : "/images/flownser_ch10.png"
        },
      ]
    },
    {
      "title" : "평생교육원",
      "sub_title" : "모든 소통은 온몸으로 합니다.",
      "flownsers" : [
        {
          "name" : "Raina",
          "info" : "raise you up",
          "explanation" : "고민보다 Go :)",
          "profile" : "/images/flownser_raina.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
        {
          "name" : "Julia",
          "info" : "줄리아 아니고 율리아",
          "explanation" : "let it go",
          "profile" : "/images/flownser_julia.jpg",
          "characters" : "/images/flownser_ch00.png"
        },
      ]
    },
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
