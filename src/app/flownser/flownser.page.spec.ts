import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlownserPage } from './flownser.page';

describe('FlownserPage', () => {
  let component: FlownserPage;
  let fixture: ComponentFixture<FlownserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlownserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlownserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
