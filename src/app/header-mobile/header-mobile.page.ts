
import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-header-mobile',
  templateUrl: './header-mobile.page.html',
  styleUrls: ['./header-mobile.page.scss'],
})
export class HeaderMobilePage {

  //mobile menu
  constructor(private menu: MenuController) { }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  openEnd() {
    this.menu.open('end');
  }
}




