import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderMobilePage } from './header-mobile.page';

describe('HeaderMobilePage', () => {
  let component: HeaderMobilePage;
  let fixture: ComponentFixture<HeaderMobilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderMobilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMobilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
